const response = {
    success : 0, //请求成功
    paramLack: 1, // 参数缺失
    paramErr: 2, // 参数格式错误

    usernameExist: 3, // 用户名已存在
    accountErr: 4, // 用户名或密码错误

    noMatch: 8, // 未找到匹配结果

    serverErr: -1, // 服务器异常
    unknown: -2, // 未知错误
}

export default response