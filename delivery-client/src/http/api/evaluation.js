import $http from "@/http/http";

const baseURL = '/evaluation'

export const AddDishEvaluation = (data => {
    return $http.post(baseURL + '/dish/add', data)
})

export const AddRestaurantEvaluation = (data => {
    return $http.post(baseURL + '/restaurant/add', data)
})

export const AddStafferEvaluation = (data => {
    return $http.post(baseURL + '/staffer/add', data)
})

export const FindDishEvaluation = (data => {
    return $http.post(baseURL + '/dish/find', data)
})

export const FindRestaurantEvaluation = (data => {
    return $http.post(baseURL + '/restaurant/find', data)
})

export const FindStafferEvaluation = (data => {
    return $http.post(baseURL + '/staffer/find', data)
})