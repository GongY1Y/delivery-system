import $http from "@/http/http";

const baseURL = 'voucher'

export const useVoucher = (data => {
    return $http.post(baseURL + '/use', data)
})