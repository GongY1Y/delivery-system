import $http from "@/http/http";

const baseURL = '/admin'

// 登录，需要username和password
export const login = (data => {
    return $http.post(baseURL + '/login', data)
})

// 获取所有用户信息
export const userList = (() => {
    return $http.get(baseURL + '/user/all')
})

// 新增一个金券，需要amount（金券数额）
export const newVoucher = (data => {
    return $http.post(baseURL + '/voucher/new', data)
})

// 获取金券列表
export const voucherList = (() => {
    return $http.get(baseURL + '/voucher/all')
})

// 删除金券：需要voucherId
export const deleteVoucher = (data => {
    return $http.post(baseURL + '/voucher/delete', data)
})