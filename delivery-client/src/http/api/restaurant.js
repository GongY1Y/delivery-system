import $http from '../http'

const baseURL = '/restaurant'

// 登录，需要username和password
export const login = (data => {
    return $http.post(baseURL + '/login', data)
})

// 创建餐厅：需要userId、餐厅名和餐厅描述
export const createRestaurant = (data => {
    return $http.post(baseURL + '/create', data)
})

// 加入餐馆，需要userId，餐馆id
export const joinRestaurant = (data => {
    return $http.post(baseURL + '/join', data)
})

// 查询餐馆，需要餐馆名
export const queryRestaurant = (data => {
    return $http.post(baseURL + '/query', data)
})

// 查询详细信息
export const getInformation = (data => {
    return $http.post(baseURL + '/information/get', data)
})

// 修改餐馆信息
export const setInformation = (data => {
    return $http.post(baseURL + '/information/set', data)
})

// 查询申请信息
export const queryApplication = (data => {
    return $http.post(baseURL + '/application/all', data)
})

// 审批入职申请
export const examineApplication = (data => {
    return $http.post(baseURL + '/examine', data)
})

// 查看员工
export const allStaffer = (data => {
    return $http.post(baseURL + '/staffer/all', data)
})

// 删除员工
export const deleteStaffer = (data => {
    return $http.post(baseURL + '/staffer/delete', data)
})

// 修改员工薪资
export const setSalary = (data => {
    return $http.post(baseURL + '/staffer/salary/set', data)
})

// 新增菜品
export const addDish = (data => {
    return $http.post(baseURL + '/dish/add', data)
})

// 查看餐馆内所有菜品
export const allDish = (data => {
    return $http.post(baseURL + '/dish/all', data)
})

// 删除菜品
export const deleteDish = (data => {
    return $http.post(baseURL + '/dish/delete', data)
})

// 获取待接取订单
export const getWaitOrder = (data => {
    return $http.post(baseURL + '/order/wait', data)
})

// 获取进行中订单
export const getProgressOrder = (data => {
    return $http.post(baseURL + '/order/progress', data)
})

// 获取已完成订单
export const getFinishOrder = (data => {
    return $http.post(baseURL + '/order/finish', data)
})

// 接取订单
export const startOrder = (data => {
    return $http.post(baseURL + '/order/start', data)
})

// 完成订单
export const endOrder = (data => {
    return $http.post(baseURL + '/order/end', data)
})