import $http from "@/http/http";

const baseURL = '/place'

// 新增地点
export const addPlace = (data => {
    return $http.post(baseURL + '/add', data)
})

// 获取所有地点信息
export const allPlaces = (() => {
    return $http.get(baseURL + '/all')
})

// 删除指定地点
export const deletePlace = (data => {
    return $http.post(baseURL + '/delete', data)
})