import $http from '@/http/http'

const baseURL = 'user'

// 注册账号，需要username和password
export const register = (data => {
    return $http.post(baseURL + '/register', data)
})

// 登录，需要username和password
export const login = (data => {
    return $http.post(baseURL + '/login', data)
})

// 获取个人信息
export const getInformation = (data => {
    return $http.post(baseURL + '/information/get', data)
})

// 修改个人信息
export const setInformation = (data => {
    return $http.post(baseURL + '/information/set', data)
})

// 查询菜品列表，需要categoryId
export const getDishList = (data => {
    return $http.post(baseURL + '/dish/get', data)
})

// 用户点餐：userId，dishId，quantity，money
export const orderDish = (data => {
    return $http.post(baseURL + '/buy', data)
})

// 获取待接取订单
export const getWaitOrder = (data => {
    return $http.post(baseURL + '/order/wait', data)
})

// 获取进行中订单
export const getProgressOrder = (data => {
    return $http.post(baseURL + '/order/progress', data)
})

// 获取已完成订单
export const getFinishOrder = (data => {
    return $http.post(baseURL + '/order/finish', data)
})

// 获取已评价订单
export const getEvaluateOrder = (data => {
    return $http.post(baseURL + '/order/evaluate', data)
})

// 获取昵称
export const getNickname = (data => {
    return $http.post(baseURL + '/nickname', data)
})