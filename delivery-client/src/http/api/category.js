import $http from "@/http/http";

const baseURL = '/category'

// 获取所有菜式
export const allCategory = (() => {
    return $http.get(baseURL + '/all')
})