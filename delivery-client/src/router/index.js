import Vue from 'vue'
import VueRouter from 'vue-router'

// 用户模块
import UserLogin from '@/views/user/Login'
import Shop from "@/views/user/Shop";

// 餐馆模块
import RestaurantLogin from '@/views/restaurant/Login'
import Common from '@/views/restaurant/Common'
import Master from "@/views/restaurant/Master";
import Staffer from "@/views/restaurant/Staffer";

// 管理员模块
import AdminLogin from '@/views/admin/Login'
import Manage from "@/views/admin/Manage";

Vue.use(VueRouter)

const routes = [
  {
    path: '/user/shop',
    name: 'Shop',
    component: Shop,
    meta: {
      title: '订餐平台'
    }
  },
  {
    path: '/restaurant/master',
    name: 'Master',
    component: Master,
    meta: {
      title: '餐馆主管理平台'
    }
  },
  {
    path: '/restaurant/staffer',
    name: 'Staffer',
    component: Staffer,
    meta: {
      title: '派送系统'
    }
  },
  {
    path: '/restaurant/common',
    name: 'RestaurantCommon',
    component: Common,
    meta: {
      title: '创建或加入餐馆'
    }

  },
  {
    path: '/restaurant/login',
    name: 'RestaurantLogin',
    component: RestaurantLogin,
    meta: {
      title: '职工登录页面'
    }
  },
  {
    path: '/',
    name: 'UserLogin',
    component: UserLogin,
    meta: {
      title: '欢迎来到订餐平台~'
    }
  },
  {
    path: '/admin/login',
    name: 'AdminLogin',
    component: AdminLogin,
    meta: {
      title: '管理员登录'
    }
  },
  {
    path: '/admin/manager',
    name: 'Manager',
    component: Manage,
    meta: {
      title: '后台管理'
    }
  }
]

const router = new VueRouter({
  routes
})

export default router
