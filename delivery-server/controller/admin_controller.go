package controller

import (
	"delivery-server/service"
	"delivery-server/utils"
	"delivery-server/utils/response"
	"encoding/json"
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/mvc"
	"github.com/kataras/iris/v12/sessions"
	"strconv"
	"time"
)

// AdminController 管理员控制器
type AdminController struct {

	// iris框架为每个请求绑定的上下文对象
	Ctx iris.Context

	// admin功能实体
	Service service.AdminService

	// session对象
	Session *sessions.Session
}

// PostLogin 管理员登录功能
// 接口：/admin/login
// username：用户名
// password：密码
func (ac *AdminController) PostLogin() mvc.Result {

	//iris.New().Logger().Info(" results login ")

	// 获取请求体参数
	username := ac.Ctx.FormValue("username")
	password := ac.Ctx.FormValue("password")

	// 数据参数检验
	if username == "" || password == "" {
		return response.ParamLack()
	}

	// 根据用户名、密码到数据库中查询对应的管理信息
	results, exist := ac.Service.AdminLoginByUsernameAndPassword(username, password)

	// 管理员不存在，即用户名或密码错误
	if !exist {
		return response.AccountErr()
	}

	// 管理员存在，获取管理员id
	adminIdStr := results[0]["id"]
	adminId, err := strconv.Atoi(adminIdStr)
	if err != nil {
		panic(err.Error())
	}

	// 设置session
	userByte, _ := json.Marshal(adminIdStr)
	ac.Session.Set("admin", userByte)

	return response.Success(adminId)
}

// GetUserAll 获取全部用户列表
// 接口：/admin/user/all
func (ac *AdminController) GetUserAll() mvc.Result {
	userList := ac.Service.GetAllUsers()
	return response.Success(userList)
}

// PostVoucherNew 新增一个金券
// 接口：/admin/voucher/new
// amount：金券面额
func (ac *AdminController) PostVoucherNew() mvc.Result {

	// 获取请求体参数：amount
	amountStr := ac.Ctx.FormValue("amount")

	// 数据参数检验
	if amountStr == "" {
		return response.ParamLack()
	}

	// 将数额由字符串转为int
	amount, err := strconv.Atoi(amountStr)
	if err != nil {
		// 参数不为数字
		return response.ParamErr()
	}

	// 获得一个随机6位验证码
	code := utils.GetRandomCapital(6)

	// 将当前时间定为创建时间
	createdTime := time.Now()

	// 添加这一行voucher数据
	isSuccess := ac.Service.AddVoucher(amount, code, createdTime)

	// 新增失败
	if !isSuccess {
		return response.ServerErr()
	}

	// 请求成功
	return response.Success(true)
}

// GetVoucherAll 获取所有代金券
// 接口：/admin/voucher/all
// page：当前页数
// size：每页数据行数
func (ac *AdminController) GetVoucherAll() mvc.Result {
	voucherList := ac.Service.GetAllVouchers()
	return response.Success(voucherList)
}

// PostVoucherDelete 删除指定id的金券数据
// 接口：/admin/voucher/delete
// voucherId：金券id
func (ac *AdminController) PostVoucherDelete() mvc.Result {

	// 获取金券id
	voucherIdStr := ac.Ctx.FormValue("voucherId")

	// 参数检验
	if voucherIdStr == "" {
		return response.ParamLack()
	}

	// 类型转换
	voucherId, err := strconv.Atoi(voucherIdStr)
	if err != nil {
		// 参数不为数字
		return response.ParamErr()
	}

	// 执行删除操作
	isSuccess := ac.Service.DeleteVoucher(voucherId)

	// 删除失败
	if !isSuccess {
		return response.ServerErr()
	}

	// 删除成功
	return response.Success(true)
}
