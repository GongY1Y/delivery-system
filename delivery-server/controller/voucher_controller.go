package controller

import (
	"delivery-server/service"
	"delivery-server/utils"
	"delivery-server/utils/response"
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/mvc"
	"github.com/kataras/iris/v12/sessions"
)

type VoucherController struct {
	Ctx     iris.Context
	Service service.VoucherService
	Session *sessions.Session
}

// PostUse 使用金券
// 接口：/voucher/use
func (vc *VoucherController) PostUse() mvc.Result {

	userId := vc.Ctx.FormValue("userId")
	voucherId := vc.Ctx.FormValue("voucherId")
	voucherCode := vc.Ctx.FormValue("voucherCode")

	notNull := utils.CheckNotNull(userId, voucherId, voucherCode)
	if !notNull {
		return response.ParamLack()
	}

	paramsInt, convertSuccess := utils.CheckInteger(userId, voucherId)
	if !convertSuccess {
		return response.ParamErr()
	}

	code := vc.Service.UseVoucher(paramsInt[userId], paramsInt[voucherId], voucherCode)
	return response.Template(code, code == response.SuccessCode)
}
