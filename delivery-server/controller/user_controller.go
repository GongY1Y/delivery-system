package controller

import (
	"delivery-server/service"
	"delivery-server/utils"
	"delivery-server/utils/response"
	"encoding/json"
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/mvc"
	"github.com/kataras/iris/v12/sessions"
	"strconv"
)

// UserController 用户控制器
type UserController struct {

	// 上下文对象
	Ctx iris.Context

	// user功能实体
	Service service.UserService

	// session对象
	Session *sessions.Session
}

// PostLogin 用户登录
// 接口：/user/login
// username：用户名
// password：密码
func (uc *UserController) PostLogin() mvc.Result {

	// 获取请求体参数
	username := uc.Ctx.FormValue("username")
	password := uc.Ctx.FormValue("password")

	// 数据参数检验
	if username == "" || password == "" {
		return response.ParamLack()
	}

	// 根据用户名和密码查询用户id
	userIdStr, exist := uc.Service.QueryUserIdByUsernameAndPassword(username, password)

	// 该用户不存在，即用户名或密码错误
	if !exist {
		return response.AccountErr()
	}

	userId, err := strconv.Atoi(userIdStr)
	if err != nil {
		panic(err.Error())
	}

	// 设置session
	userByte, _ := json.Marshal(userIdStr)
	uc.Session.Set("user", userByte)

	// 返回用户id
	return response.Success(userId)
}

// PostRegister 用户注册
// 接口：/user/register
// username；用户名
// password：密码
func (uc *UserController) PostRegister() mvc.Result {
	// 获取请求体参数
	username := uc.Ctx.FormValue("username")
	password := uc.Ctx.FormValue("password")

	// 数据参数检验
	if username == "" || password == "" {
		return response.ParamLack()
	}

	// 尝试注册
	code := uc.Service.RegisterTransaction(username, password)
	return response.Template(code, code == response.SuccessCode)
}

// PostInformationGet 查看个人信息
// 接口：/user/information/get
// userId：用户id
func (uc *UserController) PostInformationGet() mvc.Result {

	userIdStr := uc.Ctx.FormValue("userId")

	if userIdStr == "" {
		return response.ParamLack()
	}

	userId, err := strconv.Atoi(userIdStr)
	if err != nil {
		return response.ParamErr()
	}

	// 获取查询结果
	results, err := uc.Service.QueryInformation(userId)
	if err != nil {
		return response.ServerErr()
	}

	return response.Success(results[0])
}

// PostInformationSet 修改个人信息
// 接口：/user/information/set
// userId：用户id
// sex：性别 0女 1男
// placeId：地点id
// nickname：昵称
func (uc *UserController) PostInformationSet() mvc.Result {

	// 获取请求体参数
	userIdStr := uc.Ctx.FormValue("userId")
	sexStr := uc.Ctx.FormValue("sex")
	placeIdStr := uc.Ctx.FormValue("placeId")
	nickname := uc.Ctx.FormValue("nickname")

	// 参数缺失校验
	if userIdStr == "" || sexStr == "" || placeIdStr == "" || nickname == "" {
		return response.ParamLack()
	}

	// 获取用户id
	userId, err := strconv.Atoi(userIdStr)
	if err != nil {
		return response.ParamErr()
	}

	// 获取性别
	sex, err := strconv.Atoi(sexStr)
	if err != nil {
		return response.ParamErr()
	}

	// 获取地点id
	placeId, err := strconv.Atoi(placeIdStr)
	if err != nil {
		return response.ParamErr()
	}

	// 修改个人信息
	err = uc.Service.UpdateInformation(userId, sex, placeId, nickname)
	if err != nil {
		return response.ServerErr()
	}

	return response.Success(true)
}

// PostDishGet 获取餐品
// 接口：/user/dish/get
// categoryId：菜式id
func (uc *UserController) PostDishGet() mvc.Result {

	categoryId := uc.Ctx.FormValue("categoryId")

	notNull := utils.CheckNotNull(categoryId)
	if !notNull {
		return response.ParamLack()
	}

	paramsInt, convertSuccess := utils.CheckInteger(categoryId)
	if !convertSuccess {
		return response.ParamErr()
	}

	results, err := uc.Service.QueryDish(paramsInt[categoryId])
	if err != nil {
		return response.ServerErr()
	}

	return response.Success(results)
}

// PostBuy 购买菜品
// 接口：/user/buy
// userId：用户id
// dishId：餐品id
// quantity：数量
// money：总金额
func (uc *UserController) PostBuy() mvc.Result {

	userId := uc.Ctx.FormValue("userId")
	dishId := uc.Ctx.FormValue("dishId")
	quantity := uc.Ctx.FormValue("quantity")
	money := uc.Ctx.FormValue("money")

	notNull := utils.CheckNotNull(userId, dishId, quantity, money)
	if !notNull {
		return response.ParamLack()
	}

	paramsInt, convertSuccess := utils.CheckInteger(userId, dishId, quantity, money)
	if !convertSuccess {
		return response.ParamErr()
	}

	code := uc.Service.TryToBuy(paramsInt[userId], paramsInt[dishId], paramsInt[quantity], paramsInt[money])
	return response.Template(code, code == response.SuccessCode)
}

// PostOrderWait 待接取订单
// 接口：/user/order/wait
// userId：用户id
func (uc *UserController) PostOrderWait() mvc.Result {

	userIdStr := uc.Ctx.FormValue("userId")

	if userIdStr == "" {
		return response.ParamLack()
	}

	userId, err := strconv.Atoi(userIdStr)
	if err != nil {
		return response.ParamErr()
	}

	results, err := uc.Service.AllWaitOrder(userId)
	if err != nil {
		return response.ServerErr()
	}

	return response.Success(results)
}

// PostOrderProgress 正在派送的订单
// 接口：/user/order/progress
// userId：用户id
func (uc *UserController) PostOrderProgress() mvc.Result {

	userIdStr := uc.Ctx.FormValue("userId")

	if userIdStr == "" {
		return response.ParamLack()
	}

	userId, err := strconv.Atoi(userIdStr)
	if err != nil {
		return response.ParamErr()
	}

	results, err := uc.Service.AllProgressOrder(userId)
	if err != nil {
		return response.ServerErr()
	}

	return response.Success(results)
}

// PostOrderFinish 已完成订单
// 接口：/user/order/finish
// userId：用户id
func (uc *UserController) PostOrderFinish() mvc.Result {

	userIdStr := uc.Ctx.FormValue("userId")

	if userIdStr == "" {
		return response.ParamLack()
	}

	userId, err := strconv.Atoi(userIdStr)
	if err != nil {
		return response.ParamErr()
	}

	results, err := uc.Service.AllFinishOrder(userId)
	if err != nil {
		return response.ServerErr()
	}

	return response.Success(results)
}

// PostOrderEvaluate 已评价订单
// 接口：/user/order/evaluate
// userId：用户id
func (uc *UserController) PostOrderEvaluate() mvc.Result {

	userIdStr := uc.Ctx.FormValue("userId")

	if userIdStr == "" {
		return response.ParamLack()
	}

	userId, err := strconv.Atoi(userIdStr)
	if err != nil {
		return response.ParamErr()
	}

	results, err := uc.Service.AllEvaluateOrder(userId)
	if err != nil {
		return response.ServerErr()
	}

	return response.Success(results)
}

// PostNickname 获取用户昵称
// 接口：/user/nickname
// userId：用户id
func (uc *UserController) PostNickname() mvc.Result {

	userIdStr := uc.Ctx.FormValue("userId")

	if userIdStr == "" {
		return response.ParamLack()
	}

	userId, err := strconv.Atoi(userIdStr)
	if err != nil {
		return response.ParamErr()
	}

	results, err := uc.Service.GetNickname(userId)
	if err != nil {
		return response.ServerErr()
	}

	return response.Success(results[0])
}
