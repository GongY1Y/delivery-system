package controller

import (
	"delivery-server/service"
	"delivery-server/utils"
	"delivery-server/utils/response"
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/mvc"
	"github.com/kataras/iris/v12/sessions"
)

type EvaluationController struct {
	Ctx     iris.Context
	Service service.EvaluationService
	Session *sessions.Session
}

// PostDishAdd 新增餐品评价
// 接口：/evaluation/dish/add
// userId：用户id
// dishId：餐品id
// score：评分
// description：描述
func (ec *EvaluationController) PostDishAdd() mvc.Result {

	userId := ec.Ctx.FormValue("userId")
	dishId := ec.Ctx.FormValue("dishId")
	score := ec.Ctx.FormValue("score")
	description := ec.Ctx.FormValue("description")

	notNull := utils.CheckNotNull(userId, dishId, score)
	if !notNull {
		return response.ParamLack()
	}

	paramsInt, convertSuccess := utils.CheckInteger(userId, dishId, score)
	if !convertSuccess {
		return response.ParamErr()
	}

	err := ec.Service.AddDishEvaluation(paramsInt[userId], paramsInt[dishId], paramsInt[score], description)
	if err != nil {
		return response.ServerErr()
	}

	return response.Success(true)
}

// PostRestaurantAdd 新增餐馆评价
// 接口：/evaluation/restaurant/add
// userId：用户id
// restaurantId：餐馆id
// score：评分
// description：描述
func (ec *EvaluationController) PostRestaurantAdd() mvc.Result {

	userId := ec.Ctx.FormValue("userId")
	restaurantId := ec.Ctx.FormValue("restaurantId")
	score := ec.Ctx.FormValue("score")
	description := ec.Ctx.FormValue("description")

	notNull := utils.CheckNotNull(userId, restaurantId, score)
	if !notNull {
		return response.ParamLack()
	}

	paramsInt, convertSuccess := utils.CheckInteger(userId, restaurantId, score)
	if !convertSuccess {
		return response.ParamErr()
	}

	err := ec.Service.AddRestaurantEvaluation(paramsInt[userId], paramsInt[restaurantId], paramsInt[score], description)
	if err != nil {
		return response.ServerErr()
	}

	return response.Success(true)
}

// PostStafferAdd 新增员工评价
// 接口：/evaluation/staffer/add
// userId：用户id
// stafferId：员工id
// score：评分
// description：描述
func (ec *EvaluationController) PostStafferAdd() mvc.Result {

	userId := ec.Ctx.FormValue("userId")
	stafferId := ec.Ctx.FormValue("stafferId")
	score := ec.Ctx.FormValue("score")
	description := ec.Ctx.FormValue("description")

	notNull := utils.CheckNotNull(userId, stafferId, score)
	if !notNull {
		return response.ParamLack()
	}

	paramsInt, convertSuccess := utils.CheckInteger(userId, stafferId, score)
	if !convertSuccess {
		return response.ParamErr()
	}

	err := ec.Service.AddStafferEvaluation(paramsInt[userId], paramsInt[stafferId], paramsInt[score], description)
	if err != nil {
		return response.ServerErr()
	}

	return response.Success(true)
}

// PostDishFind 查询餐品评价
// 接口：/evaluation/dish/find
// dishId：餐品id
func (ec *EvaluationController) PostDishFind() mvc.Result {

	dishId := ec.Ctx.FormValue("dishId")

	if dishId == "" {
		return response.ParamLack()
	}

	paramsInt, convertSuccess := utils.CheckInteger(dishId)
	if !convertSuccess {
		return response.ParamErr()
	}

	results, err := ec.Service.QueryDishEvaluation(paramsInt[dishId])
	if err != nil {
		return response.ServerErr()
	}

	return response.Success(results)
}

// PostRestaurantFind 查询餐馆评价
// 接口：/evaluation/restaurant/find
// restaurantId：餐馆id
func (ec *EvaluationController) PostRestaurantFind() mvc.Result {

	restaurantId := ec.Ctx.FormValue("restaurantId")

	if restaurantId == "" {
		return response.ParamLack()
	}

	paramsInt, convertSuccess := utils.CheckInteger(restaurantId)
	if !convertSuccess {
		return response.ParamErr()
	}

	results, err := ec.Service.QueryRestaurantEvaluation(paramsInt[restaurantId])
	if err != nil {
		return response.ServerErr()
	}

	return response.Success(results)
}

// PostStafferFind 查询员工评价
// 接口：/evaluation/staffer/find
// stafferId：员工id
func (ec *EvaluationController) PostStafferFind() mvc.Result {

	stafferId := ec.Ctx.FormValue("stafferId")

	if stafferId == "" {
		return response.ParamLack()
	}

	paramsInt, convertSuccess := utils.CheckInteger(stafferId)
	if !convertSuccess {
		return response.ParamErr()
	}

	results, err := ec.Service.QueryStafferEvaluation(paramsInt[stafferId])
	if err != nil {
		return response.ServerErr()
	}

	return response.Success(results)
}
