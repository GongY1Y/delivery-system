package controller

import (
	"delivery-server/service"
	"delivery-server/utils/response"
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/mvc"
	"github.com/kataras/iris/v12/sessions"
	"strconv"
)

type PlaceController struct {

	// 上下文对象
	Ctx iris.Context

	// place功能实体
	Service service.PlaceService

	// session对象
	Session *sessions.Session
}

// PostAdd 新增地点
// 接口：/place/add
// name：新地点的名称
func (pc *PlaceController) PostAdd() mvc.Result {

	// 获取请求体
	name := pc.Ctx.FormValue("name")

	// 参数非空校验
	if name == "" {
		return response.ParamLack()
	}

	// 尝试新增地点
	code := pc.Service.TryToAddPlace(name)
	return response.Template(code, code == response.SuccessCode)
}

// GetAll 获取所有地点
// 接口：/place/all
func (pc *PlaceController) GetAll() mvc.Result {

	// 获取所有的地点数据
	results, err := pc.Service.AllPlaces()
	if err != nil {
		return response.ServerErr()
	}

	// 获取成功
	return response.Success(results)
}

// PostDelete 删除地点
// 接口：/place/delete
// 参数：placeId
func (pc *PlaceController) PostDelete() mvc.Result {

	// 获取请求体参数
	placeIdStr := pc.Ctx.FormValue("placeId")

	// 参数非空校验
	if placeIdStr == "" {
		return response.ParamErr()
	}

	// 获取整型地点id
	placeId, err := strconv.Atoi(placeIdStr)
	if err != nil {
		response.ParamErr()
	}

	// 删除该地点
	success, err := pc.Service.DeletePlace(placeId)
	if err != nil || !success {
		return response.ServerErr()
	}

	// 删除成功
	return response.Success(true)
}
