module delivery-server

go 1.16

require (
	github.com/Joker/hpp v1.0.0 // indirect
	github.com/go-sql-driver/mysql v1.4.1
	github.com/go-xorm/xorm v0.7.9
	github.com/kataras/iris/v12 v12.2.0-alpha2.0.20210717090056-b2cc3a287149
	github.com/nats-io/nats-server/v2 v2.3.4 // indirect
	github.com/smartystreets/goconvey v1.6.4 // indirect
)
