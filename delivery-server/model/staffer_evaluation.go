package model

import "time"

type StafferEvaluation struct {
	Id          int64     `xorm:"pk autoincr" json:"id"` // 评价id
	Score       int8      `json:"score"`                 // 评分
	Description string    `json:"description"`           // 描述
	CreatedTime time.Time `json:"created_time"`          // 评价时间

	UserId    int64 `json:"user_id"`    // 用户id
	StafferId int64 `json:"staffer_id"` // 送餐员id
}
