package model

type Staffer struct {
	Id         int64 `xorm:"pk autoincr" json:"id"`       // 员工id
	Salary     int64 `xorm:"default 0" json:"salary"`     // 工资
	Employ     bool  `xorm:"default 0" json:"employ"`     // 是否录用
	Invalidity bool  `xorm:"default 0" json:"invalidity"` // 是否无效

	RestaurantId int64 `json:"restaurant_id"` // 餐馆id
	UserId       int64 `json:"user_id"`       // 用户id
}
