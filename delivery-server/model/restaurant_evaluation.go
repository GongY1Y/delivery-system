package model

import "time"

type RestaurantEvaluation struct {
	Id          int64     `xorm:"pk autoincr" json:"id"` // 评价id
	Score       int8      `json:"score"`                 // 评分
	Description string    `json:"description"`           // 描述
	CreatedTime time.Time `json:"created_time"`          // 评价时间

	UserId       int64 `json:"user_id"`       // 用户id
	RestaurantId int64 `json:"restaurant_id"` // 餐馆id
}
