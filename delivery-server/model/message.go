package model

import "time"

type Message struct {
	Id          int64     `xorm:"pk autoincr" json:"id"`    // 消息id
	Content     string    `json:"content"`                  // 内容
	CreatedTime time.Time `json:"created_time"`             // 创建时间
	IsRead      bool      `xorm:"default 0" json:"is_read"` // 是否已读

	UserId int64 `json:"user_id"` // 用户id
}
