package model

type Place struct {
	Id         int64  `xorm:"pk autoincr" json:"id"`       // 地点id
	Name       string `xorm:"varchar(31)" json:"name"`     // 名称
	Invalidity bool   `xorm:"default 0" json:"invalidity"` // 是否无效
}
