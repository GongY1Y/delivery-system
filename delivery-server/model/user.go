package model

type User struct {
	Id       int64  `xorm:"pk autoincr" json:"id"`       // 用户id
	Username string `xorm:"varchar(31)" json:"username"` // 用户名
	Password string `xorm:"varchar(15)" json:"password"` // 密码
	Nickname string `xorm:"varchar(31)" json:"nickname"` // 昵称
	Sex      bool   `json:"sex"`                         // 性别：女0、男1
	Balance  int64  `xorm:"default 0" json:"balance"`    // 余额
	Face     string `xorm:"varchar(15)" json:"face"`     // 头像

	PlaceId int64 `json:"place_id"` // 地点id
}
