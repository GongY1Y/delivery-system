package model

type Restaurant struct {
	Id          int64  `xorm:"pk autoincr" json:"id"`       // 餐馆id
	Name        string `xorm:"varchar(31)" json:"name"`     // 名称
	Description string `json:"description"`                 // 简介
	Picture     string `xorm:"varchar(15)" json:"picture"`  // 图片
	Invalidity  bool   `xorm:"default 0" json:"invalidity"` // 是否无效
	Property    int64  `xorm:"default 0" json:"property"`   // 财产

	MasterId int64 `json:"master_id"` // 主人id
	PlaceId  int64 `json:"place_id"`  // 地点id
}
