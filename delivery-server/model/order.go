package model

import "time"

type Order struct {
	Id           int64     `xorm:"pk autoincr" json:"id"`        // 订单id
	Price        int64     `json:"price"`                        // 单价
	Quantity     int       `json:"quantity"`                     // 菜肴数量
	CreatedTime  time.Time `json:"created_time"`                 // 创建时间
	FinishedTime time.Time `json:"finished_time"`                // 完成时间
	IsEvaluate   bool      `xorm:"default 0" json:"is_evaluate"` // 是否已评价
	UserId       int64     `json:"user_id"`                      // 买家id
	RestaurantId int64     `json:"restaurant_id"`                // 餐馆id
	DishId       int64     `json:"dish_id"`                      // 菜肴id
	StafferId    int64     `json:"staffer_id"`                   // 送餐员id
	UserPlace    int       `json:"user_place"`                   // 用户地址
}
