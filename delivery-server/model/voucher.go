package model

import "time"

type Voucher struct {
	Id          int64     `xorm:"pk autoincr" json:"id"`   // 金券id
	Amount      int64     `json:"amount"`                  // 数额
	Code        string    `xorm:"varchar(31)" json:"code"` // 兑换码
	CreatedTime time.Time `json:"created_time"`            // 创建时间
	UsedTime    time.Time `json:"used_time"`               // 使用时间

	UserId int64 `json:"user_id"`						   // 使用者id
}
