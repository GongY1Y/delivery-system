package model

type Dish struct {
	Id           int64  `xorm:"pk autoincr" json:"id"`       // 菜肴id
	Name         string `xorm:"varchar(31)" json:"name"`     // 名称
	Price        int    `json:"price"`                       // 价格
	Description  string `json:"description"`                 // 简介
	Picture      string `xorm:"varchar(15)" json:"picture"`  // 图片
	Invalidity   bool   `xorm:"default 0" json:"invalidity"` // 是否无效
	CategoryId   int64  `json:"category_id"`                 // 种类id
	RestaurantId int64  `json:"restaurant_id"`               // 餐馆id
}
