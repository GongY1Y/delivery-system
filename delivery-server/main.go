package main

import (
	"delivery-server/config"
	"delivery-server/controller"
	"delivery-server/datasource"
	"delivery-server/router"
	"delivery-server/service"
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/context"
	"github.com/kataras/iris/v12/mvc"
	"github.com/kataras/iris/v12/sessions"
	"time"
)

func main() {
	app := iris.New()
	app.Use(cors)

	// 设置日志级别
	app.Logger().SetLevel("debug")

	// 项目设置
	configure(app)

	mvcHandle(app)

	err := app.Run(
		iris.Addr(":"+config.InitConfig().Port),
		iris.WithoutServerError(iris.ErrServerClosed),
		iris.WithOptimizations,
	)
	if err != nil {
		return
	}

}

// 项目配置
func configure(app *iris.Application) {

	// 配置字符编码
	app.Configure(iris.WithConfiguration(iris.Configuration{
		Charset: "UTF-8",
	}))

	// 错误配置：not found error
	app.OnErrorCode(iris.StatusNotFound, func(ctx *context.Context) {
		_, err := ctx.JSON(iris.Map{
			"code":    iris.StatusNotFound,
			"message": "Not found",
			"data":    iris.Map{},
		})
		if err != nil {
			return
		}
	})

	// 错误配置：interval server error
	app.OnErrorCode(iris.StatusInternalServerError, func(ctx *context.Context) {
		_, err := ctx.JSON(iris.Map{
			"code":    iris.StatusInternalServerError,
			"message": "Internal server error",
			"data":    iris.Map{},
		})
		if err != nil {
			return
		}
	})
}

func mvcHandle(app *iris.Application) {

	sessionManager := sessions.New(sessions.Config{
		Cookie:  "session cookie",
		Expires: 24 * time.Hour,
	})

	engine := datasource.NewMysqlEngine()

	// 管理员模块
	adminService := service.NewAdminService(engine)
	admin := mvc.New(app.Party(router.AdminPrefix))
	admin.Register(
		adminService,
		sessionManager.Start,
	)
	admin.Handle(new(controller.AdminController))

	// 用户模块
	userService := service.NewUserService(engine)
	user := mvc.New(app.Party(router.UserPrefix))
	user.Register(
		userService,
		sessionManager.Start,
	)
	user.Handle(new(controller.UserController))

	// 餐馆模块
	restaurantService := service.NewRestaurantService(engine)
	restaurant := mvc.New(app.Party(router.RestaurantPrefix))
	restaurant.Register(
		restaurantService,
		sessionManager.Start,
	)
	restaurant.Handle(new(controller.RestaurantController))

	// 地点模块
	placeService := service.NewPlaceService(engine)
	place := mvc.New(app.Party(router.PlacePrefix))
	place.Register(
		placeService,
		sessionManager.Start,
	)
	place.Handle(new(controller.PlaceController))

	// 菜式模块
	categoryService := service.NewCategoryService(engine)
	category := mvc.New(app.Party(router.CategoryPrefix))
	category.Register(
		categoryService,
		sessionManager.Start,
	)
	category.Handle(new(controller.CategoryController))

	// 金券模块
	voucherService := service.NewVoucherService(engine)
	voucher := mvc.New(app.Party(router.VoucherPrefix))
	voucher.Register(
		voucherService,
		sessionManager.Start,
	)
	voucher.Handle(new(controller.VoucherController))

	// 评价模块
	evaluationService := service.NewEvaluationService(engine)
	evaluation := mvc.New(app.Party(router.EvaluationPrefix))
	evaluation.Register(
		evaluationService,
		sessionManager.Start,
	)
	evaluation.Handle(new(controller.EvaluationController))

}

// 解决跨域问题
func cors(ctx iris.Context) {
	ctx.Header("Access-Control-Allow-Origin", "*")
	if ctx.Request().Method == "OPTIONS" {
		ctx.Header("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE,PATCH,OPTIONS")
		ctx.Header("Access-Control-Allow-Headers", "Content-Type, Accept, Authorization")
		ctx.StatusCode(204)
		return
	}
	ctx.Next()
}
