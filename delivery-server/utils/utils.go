package utils

import "math/rand"
import "strconv"

// CheckNotNull 检查参数非空
func CheckNotNull(params ...string) (notNull bool) {
	for i := 0; i < len(params); i++ {
		if params[i] == "" {
			return false
		}
	}
	return true
}

// CheckInteger 字符串到整型的转换
func CheckInteger(params ...string) (paramsInt map[string]int, success bool) {
	paramsInt = make(map[string]int)
	var err error
	for i := 0; i < len(params); i++ {
		paramsInt[params[i]], err = strconv.Atoi(params[i])
		if err != nil {
			return nil, false
		}
	}
	return paramsInt, true
}

// GetRandomCapital 获取随机大写字符
func GetRandomCapital(length int) string {
	bytes := make([]byte, length)
	for i := 0; i < length; i++ {
		bytes[i] = byte(rand.Intn(26) + 65)
	}
	return string(bytes)
}
