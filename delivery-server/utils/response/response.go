package response

import "github.com/kataras/iris/v12/mvc"

// 返回码
const (
	SuccessCode   = 0
	ParamLackCode = 1
	ParamErrCode  = 2

	UsernameExistCode = 3
	AccountErrCode    = 4

	RestaurantExistCode = 5
	IsMasterCode        = 6
	IsStafferCode       = 7

	NoMatchCode = 8

	PlaceExistCode    = 9
	CategoryExistCode = 10

	MoneyErrCode     = 11
	MoneyLackCode    = 12
	DishNotExistCode = 13
	DataErrCode      = 14

	PlaceNullCode = 15

	OrderProgressCode = 16

	ServerErrCode = -1
	UnknownCode   = -2
)

// 返回信息
var message = map[int]string{

	SuccessCode:   "请求成功",
	ParamLackCode: "参数缺失",
	ParamErrCode:  "参数类型错误",

	UsernameExistCode: "用户名已存在",
	AccountErrCode:    "用户名或密码错误",

	RestaurantExistCode: "餐厅名已存在",
	IsMasterCode:        "该用户已拥有餐厅",
	IsStafferCode:       "该用户已加入餐厅",

	NoMatchCode: "未找到匹配结果",

	PlaceExistCode:    "地点名已存在",
	CategoryExistCode: "菜式名已存在",

	MoneyErrCode:     "价格显示错误",
	MoneyLackCode:    "余额不足",
	DishNotExistCode: "该菜品不存在",
	DataErrCode:      "数据错误",

	PlaceNullCode: "用户未填写地点",

	OrderProgressCode: "订单已被接取",

	ServerErrCode: "服务器异常",
	UnknownCode:   "未知错误",
}

// getMessage 获取返回信息
func getMessage(code int) string {
	str, ok := message[code]

	if ok {
		return str
	}
	return message[UnknownCode]
}

// Template 返回值模板
func Template(code int, data interface{}) mvc.Response {
	return mvc.Response{
		Object: map[string]interface{}{
			"code":    code,
			"message": getMessage(code),
			"data":    data,
		},
	}
}

// Success 请求成功0
func Success(data interface{}) mvc.Response {
	return Template(SuccessCode, data)
}

// ParamLack 参数缺失异常
func ParamLack() mvc.Response {
	return Template(ParamLackCode, false)
}

// ParamErr 参数类型错误
func ParamErr() mvc.Response {
	return Template(ParamErrCode, false)
}

// AccountErr 用户名或密码错误
func AccountErr() mvc.Response {
	return Template(AccountErrCode, false)
}

func NoMatch() mvc.Response {
	return Template(NoMatchCode, false)
}

// ServerErr 请求失败
func ServerErr() mvc.Response {
	return Template(ServerErrCode, false)
}
