package service

import (
	"github.com/go-xorm/xorm"
	"time"
)

type EvaluationService interface {

	// AddDishEvaluation 新增餐品评论
	AddDishEvaluation(userId, dishId, score int, description string) (err error)

	// AddRestaurantEvaluation 新增餐馆评论
	AddRestaurantEvaluation(userId, restaurantId, score int, description string) (err error)

	// AddStafferEvaluation 新增员工评价
	AddStafferEvaluation(userId, stafferId, score int, description string) (err error)

	// QueryDishEvaluation 查询餐品评价
	QueryDishEvaluation(dishId int) (results []map[string]string, err error)

	// QueryRestaurantEvaluation 查询餐馆评价
	QueryRestaurantEvaluation(restaurantId int) (results []map[string]string, err error)

	// QueryStafferEvaluation 查询员工评价
	QueryStafferEvaluation(stafferId int) (results []map[string]string, err error)
}

func NewEvaluationService(engine *xorm.Engine) EvaluationService {
	return &evaluationService{
		Engine: engine,
	}
}

type evaluationService struct {
	Engine *xorm.Engine
}

// AddDishEvaluation 新增餐品评论
func (es evaluationService) AddDishEvaluation(userId, dishId, score int, description string) (err error) {
	sqlStr := "INSERT INTO dish_evaluation(user_id, dish_id, score, description, created_time)" +
		" VALUES (?, ?, ?, ?, ?)"
	_, err = es.Engine.Exec(sqlStr, userId, dishId, score, description, time.Now())
	if err != nil {
		panic(err.Error())
	}

	// 标记已评论，这里用事务更好
	sqlStr = "UPDATE `order` SET is_evaluate = 1 WHERE user_id = ? AND dish_id = ?"
	_, err = es.Engine.Exec(sqlStr, userId, dishId)
	if err != nil {
		panic(err.Error())
	}
	return
}

// AddRestaurantEvaluation 新增餐馆评论
func (es evaluationService) AddRestaurantEvaluation(userId, restaurantId, score int, description string) (err error) {
	sqlStr := "INSERT INTO restaurant_evaluation(user_id, restaurant_id, score, description, created_time)" +
		" VALUES (?, ?, ?, ?, ?)"
	_, err = es.Engine.Exec(sqlStr, userId, restaurantId, score, description, time.Now())
	if err != nil {
		panic(err.Error())
	}
	return
}

// AddStafferEvaluation 新增员工评价
func (es evaluationService) AddStafferEvaluation(userId, stafferId, score int, description string) (err error) {
	sqlStr := "INSERT INTO staffer_evaluation(user_id, staffer_id, score, description, created_time)" +
		" VALUES (?, ?, ?, ?, ?)"
	_, err = es.Engine.Exec(sqlStr, userId, stafferId, score, description, time.Now())
	if err != nil {
		panic(err.Error())
	}
	return
}

// QueryDishEvaluation 查询餐品评价
func (es evaluationService) QueryDishEvaluation(dishId int) (results []map[string]string, err error) {
	sqlStr := "SELECT score, dish_evaluation.description, user.id AS userId, created_time AS createdTime, nickname" +
		" FROM dish_evaluation, user WHERE dish_id = ? AND user.id = dish_evaluation.user_id"
	results, err = es.Engine.QueryString(sqlStr, dishId)
	if err != nil {
		panic(err.Error())
	}
	return
}

// QueryRestaurantEvaluation 查询餐馆评价
func (es evaluationService) QueryRestaurantEvaluation(restaurantId int) (results []map[string]string, err error) {
	sqlStr := "SELECT score, restaurant_evaluation.description, user.id AS userId, created_time AS createdTime, nickname" +
		" FROM restaurant_evaluation, user WHERE restaurant_id = ? AND user.id = restaurant_evaluation.user_id"
	results, err = es.Engine.QueryString(sqlStr, restaurantId)
	if err != nil {
		panic(err.Error())
	}
	return
}

// QueryStafferEvaluation 查询员工评价
func (es evaluationService) QueryStafferEvaluation(stafferId int) (results []map[string]string, err error) {
	sqlStr := "SELECT score, description, user_id, created_time" +
		" FROM staffer_evaluation WHERE staffer_id = ?"
	results, err = es.Engine.QueryString(sqlStr, stafferId)
	if err != nil {
		panic(err.Error())
	}
	return
}
