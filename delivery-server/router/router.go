package router

const (
	AdminPrefix      = "/admin"
	UserPrefix       = "/user"
	RestaurantPrefix = "/restaurant"
	PlacePrefix      = "/place"
	CategoryPrefix   = "/category"
	VoucherPrefix    = "/voucher"
	EvaluationPrefix = "/evaluation"
)
