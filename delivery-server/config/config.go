package config

import (
	"encoding/json"
	"os"
)

// AppConfig 与config.json相对应
type AppConfig struct {
	AppName string `json:"app_name"` // 项目名称
	Port    string `json:"port"`     // 端口
	Mode    string `json:"mode"`     // 模式
}

// DatabaseConfig 与config_database.json相对应
type DatabaseConfig struct {
	User     string `json:"user"`     // 用户名
	Password string `json:"password"` // 密码
	Name     string `json:"name"`     // 数据库名
}

// InitConfig 初始化服务器配置
func InitConfig() *AppConfig {

	// 读取config.json中的文件
	file, err := os.Open("config.json")
	if err != nil {
		panic(err.Error())
	}

	// 解码
	config := AppConfig{}
	err = json.NewDecoder(file).Decode(&config)
	if err != nil {
		return nil
	}
	return &config
}

// InitDatabaseConfig 初始化服务器配置
func InitDatabaseConfig() *DatabaseConfig {

	// 读取config_config.json中的文件
	file, err := os.Open("config_database.json")
	if err != nil {
		panic(err.Error())
	}

	// 解码
	config := DatabaseConfig{}
	err = json.NewDecoder(file).Decode(&config)
	if err != nil {
		panic(err.Error())
	}
	return &config
}
